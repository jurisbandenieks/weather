class Weather {
    constructor(latt, long) {
        this.token = '7a5527071cdb9138d22b47046504b2a6'
        this.latt = latt
        this.long = long
    }
    // Fetch location
    async getWeather() {
        const response = await fetch(`https://cors-anywhere.herokuapp.com/https://api.darksky.net/forecast/${this.token}/${this.latt},${this.long}?units=si`, {
        })

        return await response.json()
    }
}
